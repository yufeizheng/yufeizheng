/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package montecarlo;

import java.util.LinkedList;
import java.util.List;
import javafx.util.Pair;
import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 *
 * @author zyf
 */
public class GBM_Path implements StockPath {
    
	private double rate;
	private double sigma;
	private double S0;
	private int N;
	private DateTime startDate;
	private DateTime endDate;
	private RandomVectorGenerator rvg;
	
	public GBM_Path(double rate, int N,
			double sigma, double S0,
			DateTime startDate, DateTime endDate,
			RandomVectorGenerator rvg){
		this.startDate = startDate;
		this.endDate = endDate;
		this.rate = rate;
		this.S0 = S0;
		this.sigma = sigma;
		this.N = N;
		this.rvg = rvg;
	}

	@Override
	public List<Pair<DateTime, Double>>  getPrices() {
		double[] n = rvg.getVector();
		DateTime current = new DateTime(startDate.getMillis());
		long delta = (long)(endDate.getMillis() - startDate.getMillis())/(long)(N);
                long total_in_mili = (long)(endDate.getMillis() - startDate.getMillis());
                //System.out.println("delta   "+delta);
                Duration dur = new Duration(total_in_mili);
                double delta_in_days =  dur.getStandardDays()/(double) N;
              
                //System.out.println("delta in days  "+delta_in_days);
		List<Pair<DateTime, Double>> path = new LinkedList<Pair<DateTime,Double>>();
		path.add(new Pair<DateTime, Double>(current, S0));
		for ( int i=1; i < N; ++i){
			current.plusMillis((int) delta);
			path.add(new Pair<DateTime, Double>(current, 
					path.get(path.size()-1).getValue()*Math.exp((rate-sigma*sigma/2)*delta_in_days+sigma * n[i-1]*Math.sqrt(delta_in_days))));
		}
		return path;
	}
	

	

}

