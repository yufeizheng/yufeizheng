/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package montecarlo;

import java.util.List;
import javafx.util.Pair;
import org.joda.time.DateTime;

/**
 *
 * @author zyf
 */
public class EuropeanCallOption implements PayOut {
	
	private double K;
	
	public EuropeanCallOption(double K){
		this.K = K;
	}
        public String getOptionName(){
        
            return "European call option ";
        }
	@Override
	public double getPayout(StockPath path) {
		List<Pair<DateTime, Double>> prices = path.getPrices();
		return Math.max(0, prices.get(prices.size()-1).getValue() - K);
	}

}

