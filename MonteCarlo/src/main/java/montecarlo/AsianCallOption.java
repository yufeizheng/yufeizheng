/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package montecarlo;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import org.joda.time.DateTime;

/**
 *
 * @author zyf
 */
public class AsianCallOption implements PayOut {
	
	private double K;
	
	public AsianCallOption(double K){
            
		this.K = K;
	}
        @Override
        public String getOptionName(){
        
            return "Asian call option ";
        }
	@Override
	public double getPayout(StockPath path) {
                Stats stats = new Stats();
		List<Pair<DateTime, Double>> prices = path.getPrices();
                for (Pair<DateTime, Double> pair : prices){
                    stats.add(pair.getValue());
            
                }
                
            double avg = 0;    
            try {
                 avg = stats.getMean();
            } catch (Exception ex) {
                Logger.getLogger(AsianCallOption.class.getName()).log(Level.SEVERE, null, ex);
            }
		return Math.max(0, avg - K);
	}

}