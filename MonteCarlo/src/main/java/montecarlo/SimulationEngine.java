/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package montecarlo;

import java.io.FileWriter;
import java.io.PrintWriter;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 *
 * @author zyf
 */
public class SimulationEngine {
    private double alpha;
    private double error_tolerance;
    private int N;
    private int days;
    private DateTime startDate;
    private DateTime endDate;
    private double S0;
    private double rate;
    private double volatility;
    private PayOut payout;
    public SimulationEngine(double alpha, double error_tolerance, int N, int days,
            DateTime startDate, DateTime endDate,double S0,double rate ,double volatility, PayOut payout){
        this.alpha =alpha ;
        this.error_tolerance =error_tolerance ;
        this.N = N;
        this.days = days;
        this.startDate = startDate;
         this.endDate = endDate;
         this.S0 = S0;
         this.rate = rate;
         this.volatility = volatility;
         this.payout = payout;
    }
    
    void run_simulation() throws Exception{
        RandomVectorGenerator rvg = new NormalAntitheticGenerator(new GPU_Gaussian_Generator(N,128));
        StockPath gbm_paths = new GBM_Path( rate, N, volatility, S0,
                            startDate, endDate, rvg);
        long duration =  endDate.getMillis()-startDate.getMillis();
        Duration dur = new Duration(duration);
        long dur_in_days =  dur.getStandardDays() ; 
      //  System.out.println(dur_in_days);
        double discount_factor = Math.exp(-rate*dur_in_days);
        Stats stats = new Stats();
        double mean;
        int N_sim = 0;
        NormalDistribution standard_normal = new NormalDistribution();
        double z = standard_normal.inverseCumulativeProbability(1-alpha/2);
        while (true){
            double call_price = discount_factor * payout.getPayout(gbm_paths);
            stats.add(call_price);
             N_sim++;
            if(stats.size()> 100)    // gaurantee a minimum number of sample
                {
                    mean = stats.getMean();
                    double sample_std = stats.getSampleStd();
                    double error = sample_std*z / Math.sqrt(N_sim);
                    if(error < error_tolerance)
                        break;

                }

        }
        PrintWriter pw = new PrintWriter(new FileWriter("result.txt", true)); 

       
        pw.println(payout.getOptionName()+ mean + "   "+  N_sim+ "simulations runn");
        pw.close();
    }
    
    
    
}
