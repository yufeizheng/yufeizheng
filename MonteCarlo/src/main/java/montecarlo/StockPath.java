/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package montecarlo;

import java.util.List;
import javafx.util.Pair;
import org.joda.time.DateTime;
/**
 *
 * @author zyf
 */
public interface StockPath {
    	public List<Pair<DateTime, Double>> getPrices();
        
}
