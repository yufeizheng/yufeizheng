/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package montecarlo;

/**
 *
 * @author zyf
 */
public interface PayOut {
    public double getPayout(StockPath path);
    public String getOptionName();
}
