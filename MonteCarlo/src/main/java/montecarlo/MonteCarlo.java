/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package montecarlo;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 *
 * @author zyf
 */
public class MonteCarlo {

    /**
     * @param args the command line arguments
     */
        public static void main(String[] args) throws Exception {

           
            // TODO code application logic here
            NormalDistribution standard_normal = new NormalDistribution();
            double alpha = 0.04;
            double z = standard_normal.inverseCumulativeProbability(1-alpha/2);
            double error_tolerance = 0.01;
            int N = 100;
            int days = 252;
            DateTime startDate = new DateTime();
            DateTime endDate = startDate.plusDays(days);
            double IBM_S0 = 152.35;
            double rate = 0.0001;
            double volatility = 0.01;
            double strike1 = 165;
            double strike2 = 164;
            PayOut european_call = new EuropeanCallOption(strike1);
            PayOut asian_call = new AsianCallOption(strike2);
            
            SimulationEngine sim1 = new SimulationEngine(alpha, error_tolerance, N, days,
            startDate, endDate,IBM_S0, rate , volatility, european_call);
            sim1.run_simulation();
            
            SimulationEngine sim2 = new SimulationEngine(alpha, error_tolerance, N, days,
            startDate, endDate,IBM_S0, rate , volatility, asian_call);
            sim2.run_simulation();
                   

    }

}