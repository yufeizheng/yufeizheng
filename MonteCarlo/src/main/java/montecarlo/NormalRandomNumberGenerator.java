/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package montecarlo;
import org.apache.commons.math3.distribution.NormalDistribution;
 
/**
 *
 * @author zyf
 */
public class NormalRandomNumberGenerator implements RandomVectorGenerator {
        private int N;
    //    private double mean;
    //    private double sd;
        private NormalDistribution nd;
              
        NormalRandomNumberGenerator(int N, double mean, double sd){
            this.N = N;
            this.nd = new NormalDistribution(mean,sd);
            
        }
        
        public double[] getVector(){
            double[] vector = new double[N];
		for ( int i = 0; i < vector.length; ++i){
			vector[i] =  nd.sample();
		}
		return vector;

         
        
        };
}
